# Licensing fees and OTL contact information

<p>
        <strong>For commercial licensing, please contact:<br /></strong>Mr. John Ritter<br />
        Office of Technology Licensing, Princeton University<br />
        4 New South<br />
        Princeton, NJ 08544<br />
        phone: 609-258-1570 email: <a href="mailto:jritter@princeton.edu">jritter@princeton.edu</a></p>
    <p>
        <strong>Price list:</strong><br />
        single CPU: $5,000<br />
        second CPU: discounted 25%<br />
        third CPU: discounted 50%</p>
    <p>
        <strong>Site License Fees:</strong><br />
        Only 1 user: single CPU fee<br />
        Up to 5 users: Fee multiplier 2X<br />
        Up to 10 users: Fee multiplier 3X<br />
        Up to 15 users: Fee multiplier 4X<br />
        Unlimited users: Fee multiplier 5X</p>
    <p>
        Regardless of the number of CPUs and users per site, total fees per site shall not exceed $25,000.</p>