# Documentation

<p>A preliminary, free writeup describing the basic simplex and hypercube programs, as of October 2010, and the
        theory behind their construction, is available as arXiv: 1009.4647v2. We suggest specifically that users read or
        skim through Sec. III (pp. 6-8) and Sec. IVB (pp. 13-14) (which define the standard simplex, Kuhn simplex, unit
        hypercube, and half-side one hypecube base regions), Sec. XI (pp. 56-59), which gives a sketch of the
        algorithms, Sec. XII (pp. 59-62), which discusses test integrals and the problem of false positives in thinning,
        and Sec. XII (pp. 62-71), which describes the programs in the simplex and hypercube folders, with a detailed
        discussion of their inputs and outputs. Users should also review the "Benchmark examples and comparisons"
        section on this website: <a href="/benchmarks">Benchmarks &amp; Comparisons</a>.</p>
    <p>After the arXiv draft was written, the "hybrid" programs in cube357 and the mapping programs were added to the
        PAMIR archive, with brief descriptions given in the program comment lines. Also, the subroutine packages were
        split into separate subroutine packages for the serial and parallel programs, an output line for <i>outav=(outa+outb)/2</i>
        was added to all programs, and the output line <i>indstart</i> in the MPI programs was removed. The arXiv draft
        contains a short bibliography, which includes the papers on simplex subdivison of D. Moore, and earlier work on
        multidimensional integration of Stroud, Genz and Cools, Good and Gaskens, Lyness, and others.</p>
    <p>A book, <em>The Guide to PAMIR: Theory and Use of Parameterized Adaptive Multidimensional Integration Routines</em>,
        giving a detailed manual for use of the PAMIR programs, details of the theory behind the algorithms, and further
        references, is in press with World Scientific, with publication scheduled for January, 2013. An advertisement
        and order form for the book are available at
        <a href="http://www.worldscientific.com/worldscibooks/10.1142/8587" target="_blank">World Scientific</a>.</p>
    <p>Work on the PAMIR programs, and their documentation, was supported in part by the U. S. Department of Energy
        under grant DE-FG02-90ER40542, and also benefited from the hospitality of the Aspen Center for Physics, and from
        support by members of the Institute for Advanced Study computing staff.</p>