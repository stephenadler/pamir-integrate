# Copyright, credits, feedback & updates, licensing, and disclaimer

<p>
        <b>Copyright:</b> All programs are copyright 2010, copyright 2011 by Stephen L. Adler, with the following two
        exceptions: In cubesubs357.for and cubesubs357m.for (and the real(16) versions of these), the subroutine varsort
        is a minimal rewriting of the public domain program sort3.f given on John Mahaffy's website. In the tiling
        programs included in the simplex directories, the subroutine BestLex is the public domain program available on
        H. D. Knoble's website, with a detailed pedigree given as a comment in the program listing. I am grateful to
        Prof. Mahaffy and Prof. Knoble for permission to use these subroutines in PAMIR.</p>
    <p>
        <b>Credits:</b> In any publication making use of the PAMIR programs, acknowledgment should be made to use of
        PAMIR, with a footnote or a parenthesis referring the reader to the PAMIR website <a
                href="http://www.pamir-integrate.com">www.pamir-integrate.com</a>.</p>
    <p>
        <b>Feedback &amp; updates:</b> We have tested the programs extensively, but would like to hear about any
        problems users may find. Please send details by email to the author at <a href="mailto:adler@ias.edu">adler@ias.edu</a>.
        We will study the problem and make corrections to the program archive as needed. Updates will be noted in a
        folder "updates", which will be added to the archive when the first update is made.</p>
    <p>
        <b>Licensing:</b>  The PAMIR programs are available free for research use by researchers at not-for-profit
        colleges and universities, federally funded research facilities,  Department of Energy and National Science
        Foundation Laboratories, User Facilities, and Supercomputer Centers, and other U.S. Government agencies and
        facilities, to be used solely for internal, non-commercial research. For downloading the programs onto
        individual computers, the individual equipment owner should register. For downloading the programs onto
        institutional networks, the relevant computer manager should register. This license is non-exclusive and
        non-transferable and does NOT permit redistribution of the Fortran programs, or their translations into other
        computer programming languages, by electronic or other means; all users are required to register and obtain
        PAMIR from this website. This free use license is not a GNU General Public License.</p>
    <p>
        All other use requires payment of a licensing fee, through the Princeton University Office of Technology
        Licensing (OTL), after a 30 day free trial period, at the commercial rate.  These fees apply to downloading the
        programs for internal on-site use; licensing for commercial redistribution of the programs must be negotiated on
        a case-by-case basis with OTL. Contact information for the OTL office and a fee schedule are given <a
                href="/licensing">here</a>.</p>
    <p>
        Those interested in obtaining a commercial license must send an email request to the AUTHOR, Dr. Stephen Adler,
        at <a href="mailto:adler@ias.edu">adler@ias.edu</a>. The email request should include your full corporate
        contact information, including company email contact, and provide acknowledgement that you/your company will not
        use the PAMIR software after the 30 day trial period without execution of the license agreement and payment of
        the license fee. Once receipt of this email request has been acknowledged, you may go to the registration link
        to register and download the source code. After such 30 day period, no further use is permitted unless a license
        agreement with the Princeton University Office of Technology Licensing has been executed and the licensing fee
        specified in the <a href="/licensing">fee schedule</a> has been paid in full.</p>
    <p>
        There is no obligation by Dr. Stephen Adler, the Institute for Advanced Study, or Princeton University to
        provide maintenance, support or update services.</p>
    <p>
        <b>Disclaimer of warranty:</b>  The PAMIR Programs are provided "as is" and any and all warranties, conditions,
        representations (express or implied, oral or written), are hereby disclaimed including without limitation any
        and all implied warrantiesof quality, performance, merchantability or fitness for a particular purpose. The
        AUTHOR, Princeton University, and the Institute for Advanced Study do not warrant that the functions contained
        in the program will meet USER's requirements or that operation will be uninterrupted or error free.  The entire
        risk as to the quality and performance of the PAMIR Programs is with the USER.</p>
    <p>
        <strong>Indemnification</strong>:  USER will indemnify, defend, and hold harmless the AUTHOR, Princeton
        University, and the Institute for Advanced Study, their trustees, officers, faculty, employees, members,
        visitors, students, and agents, from and against any and all actions, suits, claims, proceedings, demands,
        prosecutions, liabilities, costs, expenses, damages, deficiencies, losses or obligations (including attorneys'
        fees and costs) based on, arising out of, or relating to USER’s performance under the Licensing Agreement,
        including, without limitation USER’s use of the PAMIR Programs and any asserted violations of the Export Control
        Laws by USER.</p>
    <p> Application of the PAMIR Programs to problems, the incorrect solution of which may result in personal injury or
        loss of property, is at your own risk. USER acknowledges and agrees that the AUTHOR, the Institute for Advanced
        Study, and Princeton University shall not be liable for any direct, indirect, special, incidental, or punitive
        damages, including lost profits resulting from use of the PAMIR Programs.</p>