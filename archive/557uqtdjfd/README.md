# Download

<p>Download the PAMIR archive of fortran programs. For details of the
        programs, see the <a href="../../INTRODUCTION.md">Introduction</a>, <a href="../../DOCUMENTATION.md">Documentation</a>, and <a
                href="https://gitlab.com/stephenadler/pamir-integrate/blob/master/benchmarks.pdf">Benchmark examples</a> sections.
    <p>Compressed archives with the same name are identical. One is a tar gzip file and the other a zip file.&nbsp;On
        Windows, both formats can be uncompressed using&nbsp;7-zip, which is available free on the Internet. Unix and
        Mac systems should be be able to&nbsp;uncompress the files without an additional&nbsp;download.</p>
    <p>A revised archive pamir_archive2 differs from the original only in making the arrays ivnew (and in simplex4 also
        fnew) allocatable arrays, rather than variable dimension arrays.&nbsp; This eliminates a problem encountered
        with the Intel conpiler (ifort) for large values of dimension ip.&nbsp;</p>
    <p>There are also minor editorial revisions to the folders readme and constant_jacobian_map.</p>