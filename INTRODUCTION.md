# Introduction

<p>PAMIR is a suite of numerical integration programs for
        multidimensional numerical integration in general dimension <i>p</i>,
        intended for use by physicists, applied mathematicians, computer
        scientists, and engineers. The programs are capable of following
        localized peaks and valleys of the integrand, by subdividing the
        integration region by a 2<i><sup>p</sup></i> subdivision at each
        stage of refinement. An adaptive mesh refinement or "thinning"
        option allows adaptive selection of subregions to be further
        subdivided, while not further dividing subregions that obey an error
        criterion. We have obtained good results from these programs in
        dimensions up to <i>p=7</i> on a personal computer, and in dimensions
        <i>p=7</i> and <i>p=9</i> on a cluster. On large clusters with ample
        running time, integrations over higher dimensions than <i>p=9</i>
        are feasible. The capabilities of the PAMIR programs are illustrated
        through benchmark examples and comparisons given on this website:
        <a href="/benchmarks">Benchmarks &amp; Comparisons</a>.</p>
    <p>A
        book, ``The Guide to PAMIR: Theory and Use of Parameterized Adaptive
        Multidimensional Integration Routines'', giving a detailed manual
        for use of the PAMIR programs, details of the theory behind the
        algorithms, and further references,  has been published by World
        Scientific.  An advertisement and order form for the book are
        available at <a
                href="http://www.worldscientific.com/worldscibooks/10.1142/8587"
                target="_blank">World Scientific</a>.</p>
    <p>The programs focus on
        base regions that are a simplex, a hypercube, or a hyper-rectangle.
        The programs are organized into folders cube13, cube357, cube357_16,
        cube579, cube579_16, simplex123, simplex4, simplex579, simplex579_16,
        loop_examples, readme, and constant_jacobian_map. The numbers in
        each folder name indicate the orders of integration routines that
        are included, for example, in cube579, hypercube integration routines
        of order 5, 7, and 9 are implemented. Similarly, in simplex579
        simplex integration routines of order 5, 7, and 9 are implemented,
        for both standard and Kuhn simplexes. The subscript _16 indicates
        that these folders contain quadruple precision (real(16)) versions
        of the default double precision programs. The folder cube357 contains
        "hybrid" programs that have an option for subdividing at each stage
        only some number <i>p<sub>1</sub></i><em>&lt; p</em> of the dimensions
        of a hyper-rectangle, chosen according to various criteria, and of
        choosing whether to use Monte-Carlo or higher order integration to
        evaluate each subregion. The folder loop_examples contains examples
        of looping over parameter values in the integrand. The folder readme
        contains instructions for changing the preset sampling parameters
        for higher order integration, and need not be accessed if the default
        parameters are used. Finally, the folder constant_jacobian_map
        contains main programs implementing a constant jacobian transformation
        from simplex to hypercube and vice versa; these did not perform
        well in our tests and so have been removed from the cube and simplex
        directories and grouped separately for users who may wish to further
        test them.</p>
    <p>The higher order integration routines use sampling
        of the integrand at points which are adjustable parameters, with
        integration weights which are computed internally; no tables of
        fixed nodes and weights appear in the programs. To get a different
        sampling, one can change the parameter values within specified
        limits, as described in the comment lines for the setparam subroutines
        and the folder readme.</p>
    <p>The simplex folders also contain
        programs for integrating over hypercubes by tiling them with Kuhn
        simplexes, which are useful for dimensions up to to <i>p=7</i>
        (beyond this, the combinatorial factor of <i>p!</i> is too costly,
        and the direct cube programs should be used.) Conversely, the cube
        folders (except for cube13) contain programs for mapping simplexes
        into hypercubes by the widely used polynomial map given in Stroud's
        classic book, which performed well in our tests.</p>
    <p>Each folder
        contains a number of main programs, and two subroutine packages.
        Main programs that do not end in "m.for" or "r.for" implement a
        first stage of subdivision up to a specified limit, governed by
        available memory. Main programs ending in "r.for" take the residual
        subregions left at the end of this first stage of subdivision and
        process them sequentially (that is, they "recirculate" the first
        stage program), thus exploiting machine speed as well as machine
        memory. Main programs ending in "m.for" are a MPI (Message-Passing
        Interface) adaptation of the "r.for" programs, in which the residual
        subregions are distributed in groups to the available parallel
        processes. To obtain an executable program, <b>one</b> main program
        should be compiled and linked to <b>one</b> of the two subroutine
        packages. The serial main program packages with names ending in
        "NN.for" and "NNr.for" should be linked to the subroutine package
        ending in "NN.for", with "NN" an integration order number. The MPI
        parallel main program packages with names ending in "NNm.for" should
        be linked to the corresponding subroutine package ending in
        "NNm.for".</p>
    <p>The programs are all given as Fortran source code.
        The function to be integrated is entered in a subroutine <i>fcn(ip,x)</i>
        [or in certain indicated cases, <i>fcn1(ip,x)</i>] in the main
        program package, with <i>ip</i> the spatial dimension and <i>x(1:ip)</i>
        the integration variable. As set up, each main program package
        contains a one line test example in <i>fcn</i> [or <i>fcn1</i>];
        users can either remove this example and type or paste in their own
        function, or remove the entire subroutine <i>fcn</i> [or <i>fcn1</i>]
        and link to their own <i>fcn(ip,x)</i> [or <i>fcn1(ip,x)</i>],
        written with the same type declarations as in the example.</p>
    <p>The spatial dimension <i>ip</i> and various options for evaluating
        the integral are entered in the main program proper, which appears
        at the end of the main program package. Both the header of the main
        program package, and various programs in it, contain comment lines
        giving instructions. The choice of sampling parameters for higher
        order integration, and of the comparison function used for thinning,
        are also made in the main program package. Under normal use the
        subroutine packages do not have to be accessed by the user. If users
        wish to modify the subroutines in any way, we strongly recommend
        doing a few test integrals before and after the modification. Since
        users will be editing the main program package, we recommend keeping
        an original copy from the download on hand, in case the copy being
        edited is spoiled.</p>
    <p>The programs use Fortran 77 conventions
        for comments and do loops, but also use Fortran 90 constructs such
        as allocatable memory and array constructors. The total length of
        the programs in all directories is about 1.1 MB. The double precision
        serial versions compile and run on the Institute for Advanced Study
        Linux system with the Intel (compile command: ifort), Portland Group
        (compile command: pgf90) and GNU version 4.1.2 (compile command:
        gfortran) Fortran compilers. The real(16) versions compile and run
        with the Intel compiler, and the MPI versions compile and run with
        the Intel MPI compiler front end (compile command: mpif90) as well
        as in cluster operation. (The Linux systems on which the MPI programs
        were tested have Open MPI installed.) On the author's various
        personal computers, the real(8) programs compile and run with the
        Digital Visual Fortran and Intel Visual Fortran packages for Microsoft
        Windows. (Please note: some of the descriptive names used in this
        paragraph are registered Trademarks of the vendor companies.)</p>