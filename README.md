# PAMIR (Parameterized Adaptive Multidimensional Integration Routines)

PAMIR is a suite of programs for multidimensional numerical
integration in general dimension <em>p</em>. It is capable of
following localized peaks and valleys of the integrand by
2<em><sup>p</sup></em> subdivision at each stage of adaptive mesh
refinement. Each program has serial versions, and a Message-Passing
Interface (MPI) version for cluster use.

* [Introduction](INTRODUCTION.md)
* [Documentation](DOCUMENTATION.md)
* [Updates](UPDATES.md)
* [Copyright, credits, feedback & updates, licensing, and disclaimer](COPYRIGHT.md)
* [Licensing fees and OTL contact information](LICENSING.md)
* [Benchmark examples and comparisons](benchmarks.pdf)
* [Register to Download](https://docs.google.com/forms/d/e/1FAIpQLSeHKGxSCH89yettSW8v9u6pcSa-SZGq4g2BPdZIQZqaDXFwUQ/viewform?usp=sf_link)
