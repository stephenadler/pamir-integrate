# Updates

<p>A revised archive pamir_archive2 differs from the original only in making the arrays ivnew (and in simplex4 also fnew) allocatable arrays, rather than variable dimension arrays.  This eliminates a problem encountered with the Intel compiler (ifort) for large values of dimension ip. </p>
    <p>There are also minor editorial revisions to the folders readme and constant_jacobian_map. </p>
    <p>***********************************************************</p>
    <p>The statements to set the random seed in the cube357 programs do not in fact set the seed value.<br />
        On the Intel compiler (ifort), the programs as written pick a machine supplied constant seed, not related<br />
        to the value of ``iseed'', and give the same random number sequence in every run.  To get a machine-<br />
        supplied seed from the wallclock, which changes in each run, change the statement<br />
        ``if(imc.ne.0) call random_seed(iseed)''<br />
        to read<br />
        ``if(imc.ne.0) call random_seed''.</p>
    <p> To get a user-supplied seed (e.g, 54321),<br />
        (1) after ``implicit integer(i-l)''   add the line  ``integer, dimension( : ), allocatable : : iseed''<br />
        (2)  Instead of the two lines specifying iseed and calling random_seed use the following:</p>
    <p>       allocate(iseed(1:8))<br />
        iseed(:)= 54321<br />
        if(imc.ne.0) call random_seed(PUT=iseed)
    </p>
    <p>*********************************************************</p>
    <p>Note that the base region for the ``cube'' programs is a half-side 1 hypercube.  To use these to integrate<br />
        a function`` f(x)'' defined in a unit hypercube, a change of variable must be included in the function  ``fcn'',<br />
        as follows (comment lines omitted):  </p>
    <p>	function fcn(ip,z)<br />
        implicit real(8)(a-h,m-z)<br />
        implicit integer(i-l)<br />
        external f<br />
        dimension x(1:ip),z(1:ip)<br />
        x(1:ip)=.5d0*(1.d0+z(1:ip))<br />
        fcn=f(x)<br />
        return<br />
        end  </p>